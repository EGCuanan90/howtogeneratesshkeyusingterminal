How to Generate an SSH Key using Terminal:

* Open your Terminal and then type cd "C:\Users\EllantEladar-PC\Downloads"

* Next is type mkdir Instructions

* Next is type cd Instructions

* Next is type echo HowToGenerateSSHKeyUsingTerminal: > HowToGenerateSSHKey.txt

* Next is type the neccessary instructions and make sure to save before exiting

* Next is go to your root file in my example is: C:\Users\EllantEladar-PC

* Next is type ssh-keygen -t rsa -C "egcuanan.srbootcamp@gmail.com"

* Just leave it blank by pressing Enter and again hit Enter for the second confirmation

* It Will Generate the following identification and puublic key to a specific path with the image

* Next is copy the path on the terminal and open that path to your local file explorer on the address bar section

* Examples are: C:\Users\EllantEladar-PC/.ssh/id_rsa.pub

* It will display to open some sort of default application just be sure to select Notepad by hitting the down arrow to select an application to run and open the .pub file

* Copy and paste the inside

* Next is Go back to your GitLab or GitHub Account and then paste this .pub file on the Add New Key button

* Next is paste the codes on .pub to the Add An SSH Key Input Section

* Next is Add a Title to recognize the key as to what kind of device and make sure the usage type is Authentication & Signing

* Next is Hit the Add Key Button and You're Done

================================================================================

You can as well use that to the ssh-keygen -t ed25519 -C "egcuanan.srbootcamp@gmail.com"

Or another ssh-keygen file type